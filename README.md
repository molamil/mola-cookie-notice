# Cookie notice component for vue.js 2.0

A cookie notice component for vue.js 2.0

## Install

Add dependency in package.json:

```
"mola-cookie-notice": "git+https://git@bitbucket.org/molamil/mola-cookie-notice.git#v1.0.3"
```

## Requirements

* [Vue.js](https://github.com/vuejs/vue) `2.x`


## Usage

App.vue

```html
<template>
    <div id="app">
        <router-view></router-view>

        <div class="cookie-notice">
            <MolaCookieNotice
                cookieName="mola-cookie-notice-top-3"
                text="Vi anvender cookies for at samle statistik og forbedre brugeroplevelsen. Ved brug af hjemmesiden accepteres cookies.">
            </MolaCookieNotice>
        </div>
    </div>
</template>

<script>
    import MolaCookieNotice from 'MolaCookieNotice'

    export default {
        name: 'app',
        data() {
            return { }
        },
        components: {
            MolaCookieNotice,
        },
    }
</script>

<style lang="scss">
    // Scope component styling using a wrapper div
    .cookie-notice {
        // Component
        .mola-cookie-notice {
            background: #000;
            .mola-cookie-notice__text {
                margin: 10px 5px 10px 14px;
                h5, p, a {
                    font-family: 'Avenir', Helvetica, Arial, sans-serif;
                    color: #fff;
                }
                h5 {
                    padding-bottom: 10px;
                    font-size: 20px;
                }
                p {
                    font-size: 13px;
                    max-width: 640px;
                }
                a {
                    text-decoration: underline;
                }
            }
            .mola-cookie-notice__buttons {
                .mola-cookie-notice__accept, .mola-cookie-notice__deny {
                    a {
                        display: block;
                        font-family: 'Avenir', Helvetica, Arial, sans-serif;
                        font-size: 14px;
                        line-height: 1.5;
                        color: #fff;
                        font-weight: bold;
                        white-space: nowrap;
                        margin: 10px;
                        padding: 10px 15px;
                        border-radius: 20px;
                        transition: all 0.15s linear;
                        text-decoration: none;
                        &:hover {
                            transition: all 0.15s linear;
                        }
                    }
                }
                .mola-cookie-notice__accept {
                    a {
                        padding: 10px 20px;
                        border: 1px solid #fff;
                        &:hover {
                            background: #fff;
                            color: #000;
                        }
                    }
                }
                .mola-cookie-notice__deny {
                    a:hover {
                        opacity: .7;
                    }
                }
            }
        }
    }
</style>

```

## Properties / Options

|Name|Type|Default|Required|Description
|:---|---|---|---|---|
|`acceptText`|String|`Accepter`||Text on button which accepts cookies and hide the cookie notice.|
|`denyText`|String|`✕`||Text on button which denies cookies and hide the cookie notice.|
|`cookieName`|String||Yes|The name of the cookie, fx. `website-name-mola-cookie-notice`.|
|`:development`|Boolean|`false`||When set to true the notice will always appear. Use when developing css.|
|`headline`|String|`''`|||
|`linkHref`|String|`''`||An url for a "Read more" link.|
|`linkText`|String|`''`||A link text for a "Read more" link.|
|`placement`|String|`top`||Cookie notice can be placed at `top` or `bottom`.|
|`text`|String||Yes||
|`transition`|String|`''`||Use any transition name and add css in the parent component or use built in `fade` or `shrink` options.|
|`:showCookieNotice`|Boolean|`false`||Used to control whether the cookie notice should be shown. The cookie notice already shows and hides based on if the user has clicked accept og deny, but it can't see if the cookie has been deleted. So here is an option to parse data about that from the parent.|
|`v-on:cookies-accepted`|String|||A function in the parent component which is called when the action button is clicked, and cookies are accepted. Good for adding tracking.|
|`v-on:cookies-denied`|String|||A function in the parent component which is called when the close button is clicked, and cookies are denied.|


## Example with all options

```html
<mola-cookie-notice
    acceptText="Så ved jeg det..."
    denyText="Luk"
    cookieName="mola-cookie-notice-bottom"
    :development="true"
    headline="COOKIE- OG PRIVATLIVSPOLITIK PÅ DR.DK"
    linkHref="http://www.fak.dk/pages/cookieinfo.aspx"
    linkText="Læs mere"
    placement="bottom"
    text="Vi bruger cookies for at forbedre din oplevelse, vurdere brugen af de enkelte elementer på dr.dk og til at støtte markedsføringen af vores services. Ved at benytte dr.dk accepterer du vores brug af cookies."
    transition="fade"
    :showCookieNotice="!$store.state.cookiesAccepted"
    v-on:cookies-accepted="cookiesAccepted"
    v-on:cookies-denied="cookiesDenied">
</mola-cookie-notice>
```

## Release a new version by adding git tag

```
$ git tag -a v1.0.3
$ git push origin master
$ git push --tags origin master
```
